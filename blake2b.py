'''
Blake2b hash simple implementation by Henry Veng
This follows the pseudo-code provided in the wikipedia page:
https://en.wikipedia.org/wiki/BLAKE_(hash_function)
This code assumes unkeyed, unsalted, sequential hash and outputs
ONLY the 512 bit hash.
'''

# Initialization vector, MUST STAY CONSTANT
IV = [
	0x6a09e667f3bcc908,
	0xbb67ae8584caa73b,
	0x3c6ef372fe94f82b,
	0xa54ff53a5f1d36f1,
	0x510e527fade682d1,
	0x9b05688c2b3e6c1f,
	0x1f83d9abfb41bd6b,
	0x5be0cd19137e2179
]

# Message permutations
SIGMA = [
	[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
	[14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3],
	[11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4],
	[7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8],
	[9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13],
	[2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9],
	[12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11],
	[13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10],
	[6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5],
	[10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0]
]

# 64 bit rotate left
def rol64b(x, n):
	temp = x >> (64 - n)
	x = (x << n) & 0xFFFFFFFFFFFFFFFF
	x = x | temp
	return x

# 64 bit rotate right
def ror64b(x, n):
	temp = (x << (64 - n)) & 0xFFFFFFFFFFFFFFFF
	x = x >> n
	x = x | temp
	return x

# convert string to int
def strToInt(s):
	s = s.encode('utf-8')
	return int.from_bytes(s, byteorder='little', signed=False)

# mixing function g
def mix(va, vb, vc, vd, x, y):
	va = (va + vb + x) & 0xFFFFFFFFFFFFFFFF
	vd = ror64b((vd ^ va), 32)
	
	vc = (vc + vd) & 0xFFFFFFFFFFFFFFFF
	vb = ror64b((vb ^ vc), 24)

	va = (va + vb + y) & 0xFFFFFFFFFFFFFFFF
	vd = ror64b((vd ^ va), 16)

	vc = (vc + vd) & 0xFFFFFFFFFFFFFFFF
	vb = ror64b((vb ^ vc), 63)
	return [va, vb, vc, vd]

# compression function f
def compress(h, chunk, t, isLastBlock):
	# v[0..7] = h[0..7]
	# v[8..15] = IV[0..7]
	v = h.copy() + IV.copy()
	
	# v[12] ^ lo(t)
	# v[13] ^ hi(t)
	v[12] = v[12] ^ (t & 0xFFFFFFFFFFFFFFFF)
	v[13] = v[13] ^ (t>>64)
	
	# invert v[14] if last block
	if isLastBlock:
		v[14] = v[14] ^ 0xFFFFFFFFFFFFFFFF
	
	# split the message "chunk" (or block) into sixteen 64 bit integer representations
	# note: This needs to be done by first sectioning the string into 64 bit (8 byte) pieces
	#       then converting each piece into an integer in little endian form (strToInt does this)
	#       e.g. chunk = "Hello World!"
	#            split: |Hello Wo|rld!|0|0|...|0| (IMPORTANT: if a piece is less than 8bytes, zero pad AFTER conversion)
	#            convert(little endian): |0x 6f 57 20 6f 6c 6c 65 48|0x 00 00 00 00 21 64 6c 72|0|0|...|0|
	m = []
	for i in range(16):
		stringPiece = chunk[8*i:8*i+8]
		piece = strToInt(stringPiece)
		m.append(piece)

	# Do the cha cha rounds; 12 rounds for blake2b
	for i in range(12):
		s = SIGMA[i%10]
		
		[v[0], v[4], v[8], v[12]] = mix(v[0], v[4], v[8], v[12], m[s[0]], m[s[1]])
		[v[1], v[5], v[9], v[13]] = mix(v[1], v[5], v[9], v[13], m[s[2]], m[s[3]])
		[v[2], v[6], v[10], v[14]] = mix(v[2], v[6], v[10], v[14], m[s[4]], m[s[5]])
		[v[3], v[7], v[11], v[15]] = mix(v[3], v[7], v[11], v[15], m[s[6]], m[s[7]])
		
		[v[0], v[5], v[10], v[15]] = mix(v[0], v[5], v[10], v[15], m[s[8]], m[s[9]])
		[v[1], v[6], v[11], v[12]] = mix(v[1], v[6], v[11], v[12], m[s[10]], m[s[11]])
		[v[2], v[7], v[8], v[13]] = mix(v[2], v[7], v[8], v[13], m[s[12]], m[s[13]])
		[v[3], v[4], v[9], v[14]] = mix(v[3], v[4], v[9], v[14], m[s[14]], m[s[15]])
	
	# combine(xor) upper and lower halves of v into h
	for i in range(16):
		h[i%8] = h[i%8] ^ v[i]
	return h

# blake2b digest function
def blake2bhash(message):
	cbMessageLen = len(message)
	cbHashLen = 64
	h = IV.copy()
	
	# h[0] ^ 0x0101kknn
	# kk = 0 since we aren't doing keyed hashing
	# nn = 64 = 0x40 since we're only doing 512 bit (64 byte) digest in this implementation
	h[0] = h[0] ^ (0x01010000 + cbHashLen)
	
	cBytesCompressed = 0
	cBytesRemaining = cbMessageLen

	# while not last byte, get next 128 byte chunk and compress
	while(cBytesRemaining > 128):
		chunk = message[cBytesCompressed:(cBytesCompressed + 128)]
		cBytesCompressed += 128
		cBytesRemaining -= 128

		h = compress(h, chunk, cBytesCompressed, False)
	
	# compress the last chunk
	chunk = message[cBytesCompressed:(cBytesCompressed + cBytesRemaining)]
	cBytesCompressed = cBytesCompressed + cBytesRemaining
	h = compress(h, chunk, cBytesCompressed, True)
	
	# convert all elements of h into little endian
	for i in range(8):
		tempBytes = h[i].to_bytes(8,'big',signed=False)
		tempInt = int.from_bytes(tempBytes,'little',signed=False)
		h[i] = tempInt

	# build the digest
	digest = 0
	for i in range(8):
		digest = (digest << 64) + h[i]
	
	return digest

# main function 
def main():
	message = input("Message to be hashed: ")
	print("The message is: " + message)
	digest = blake2bhash(message)
	print("The hash is: " + str(hex(digest)))

if __name__ == '__main__':
	main()
