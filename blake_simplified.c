#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <string.h>
#include <inttypes.h>


/*
    Constant 2-D Array that signifies the order of mixing of messages 
    for the compression function. This was obtained from the blake2s 
    psuedocode resources. 
*/
static uint32_t SIGMA[10][16] = {
    { 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15},
    {14, 10,  4,  8,  9, 15, 13,  6,  1, 12,  0,  2, 11,  7,  5,  3},
    {11,  8, 12,  0,  5,  2, 15, 13, 10, 14,  3,  6,  7,  1,  9,  4},
    { 7,  9,  3,  1, 13, 12, 11, 14,  2,  6,  5, 10,  4,  0, 15,  8},
    { 9,  0,  5,  7,  2,  4, 10, 15, 14,  1, 11, 12,  6,  8,  3, 13},
    { 2, 12,  6, 10,  0, 11,  8,  3,  4, 13,  7,  5, 15, 14,  1,  9},
    {12,  5,  1, 15, 14, 13,  4, 10,  0,  7,  6,  3,  9,  2,  8, 11},
    {13, 11,  7, 14, 12,  1,  3,  9,  5,  0, 15,  4,  8,  6,  2, 10},
    { 6, 15, 14,  9, 11,  3,  0,  8, 12,  2, 13,  7,  1,  4, 10,  5},
    {10,  2,  8,  4,  7,  6,  1,  5, 15, 11,  9, 14,  3, 12, 13,  0}
};

/* 
    Constant initialisation vectors. These values are obtained by taking
    the first 3,2 bits of the fractional parts of the square root of the
    first eight prime numbers
*/ 
static const uint32_t IV[8] = {
    0x6a09e667UL,
    0xbb67ae85UL,
    0x3c6ef372UL,
    0xa54ff53aUL,
    0x510e527fUL,
    0x9b05688cUL,
    0x1f83d9abUL,
    0x5be0cd19UL
};

void mix(uint32_t* va, uint32_t* vb, uint32_t* vc, uint32_t* vd, uint32_t m1, uint32_t m2);
uint32_t * compress(uint32_t internalHash[], uint32_t wordMessage[], uint64_t nBytesCompressed, _Bool finalBlock);
uint32_t load32(char * message);

/*
    Simplied version of Blake 2s hash algorithm
    To be used in embedded processor + programming logic hardware for UNSW COMP4601 Project 2020 semester 1
    Input: Message length of max size 2^64 bytes in length.
    Output: 32 byte hash of the message
    Paramater specification: tools.ietf.org/html/rfc7693
    Disclaimer: We are using a simplified version of Blake 2s and thus our output will not have the exact hash value as production implementation
                The code focuses on simulating the Blake2s algorithm to see if speedup can be achieved in hardware.
*/ 

int main (int argc, char *argv[]) {
    char* message = argv[1]; // The input message
    // char* messageHash; // The return hash
    uint32_t internalHash[8];

    // Check number of input arguments
    if (argc != 2) {
        printf("Incorrect arguments. Please enter a message up to 2^64 bytes in length\n");
        return 0;
    }

    // Initialise the internal hash to IV vector XOR'd with 0x0101kknn
    // In our simplified algorightm:
    // kk: a hash key which we will not be using
    // nn: Number of desired hash length (32 in hexademical
    // 0101 constant was determined as the best starting value by Blake author
    uint32_t i;
    for(i = 0; i< 8; i++ ){
        if(i == 0) {
            internalHash[i] = IV[i] ^ 0x01010020;
        } else {
            internalHash[i] = IV[i];
        }
    }

    // Each time we compress, we record how many bytes have been compressed
    uint64_t nBytesCompressed = 0;

    // Each ascii character is 1 byte or 8 bits.
    uint64_t nBytesRemaining = strlen(message);
    /* Blake2 doesn't count the null terminator
    if (nBytesRemaining == 0) {
        // Empty strings still contain a null terminator
        nBytesRemaining++;
    }
    */

    // Compress each 64-byte block chunk of the message except for the last chunk
    while (nBytesRemaining > 64) {
        uint32_t wordMessage[16];

        // Split the 64 byte block into 32-bit words so the xor function has the correct word length.
        for(i = 0; i < 16; i++) {
            // Get a 32 bit word from the message at position of i
            wordMessage[i] = load32(message + i*sizeof(wordMessage[i]));
        }
        // counters are updated prior to compression
        nBytesRemaining -= 64;
        nBytesCompressed += 64;
        compress(internalHash, wordMessage, nBytesCompressed, 0);
        // Move the message pointer up one block
        message = message + 64;
    }

    // Pad the remaining chunk with 0s and then compress
    uint32_t paddedWordMessage[16];
    for(i = 0; i < 16; i++) {
        if (nBytesRemaining >= 4) {
            nBytesCompressed += 4;
            nBytesRemaining -= 4;
            paddedWordMessage[i] = load32(message);
            message += 4;
        } else if (nBytesRemaining > 0) {
            nBytesCompressed += nBytesRemaining;
            nBytesRemaining = 0;
            paddedWordMessage[i] = load32(message);
        } else {
            paddedWordMessage[i] = 0;
        }
    }

    /*
    for(i = 0; i < 16; i++) {
        printf("%x, ",paddedWordMessage[i]);
    }
    printf("\n");
    */
    compress(internalHash, paddedWordMessage, nBytesCompressed, 1);

    for(i = 0; i < 8; i ++) {
        printf("%x", internalHash[i]);
    }
    printf("\n");
    return 0;
}

/*
    This function takes in the current state of the hash and compresses the message
    provided into the hash state. The function produces different outputs if the message
    block is the final block in the entire string. 
*/

uint32_t * compress(uint32_t internalHash[], uint32_t wordMessage[], uint64_t nBytesCompressed, _Bool finalBlock) {
    // workvector will be used to handle to store the compression vector
    uint32_t workVector[16];

    // counters used in for looops 
    uint32_t i;

    // initialise the workvector with the hash and the initialisation vector 
    for (i = 0; i < 8; i++) {
        workVector[i] = internalHash[i];
    }
    for (i = 8; i < 16; i++) {
        workVector[i] = IV[i-8];
    }

    // Twelfth element is XORd with the low(nBytesCompressed) 
    workVector[12] = (uint32_t)( workVector[12] ^ ((nBytesCompressed << 32) >> 32) );
    // Thirteenth element is XORd with the high(nBytesCompressed) 
    workVector[13] = (uint32_t)( workVector[13] ^ ( nBytesCompressed >> 32) );
    if (finalBlock) {
        workVector[14] = workVector[14] ^ 0xffffffff;
    }

    // copy over the message sent to the program and store in message
    uint32_t message[16];
    memcpy(message, wordMessage, 16*sizeof(uint32_t));
    //printf("%d, %d\n", message[0], message[1]);
    //printf("%d, %d\n", wordMessage[0], wordMessage[1]);

    // Iterate 10 times as blake2s has 10 rounds of mixing for generating new hash
    // sig vector will store each list order in SIGMA 
    // the list order from SIGMA defines the word number in the message vector we got
    uint32_t *sig;
    for (i = 0; i < 10; i++ ) {

        // Assign the list of orders from SIGMA to sig
        // i % 10 allows us to go through all 12 rounds and have a order list for every round
        // orders list 0 & 1 are used twice
        sig = SIGMA[i % 10];

        // The order of parameters is taken from the psuedocode for blake2s
        // sig vector is used to choose the appropriate word from chunk in the given order in SIGMA
        mix(&workVector[0], &workVector[4], &workVector[8], &workVector[12], message[sig[0]], message[sig[1]]);
        mix(&workVector[1], &workVector[5], &workVector[9], &workVector[13], message[sig[2]], message[sig[3]]);
        mix(&workVector[2], &workVector[6], &workVector[10], &workVector[14], message[sig[4]], message[sig[5]]);
        mix(&workVector[3], &workVector[7], &workVector[11], &workVector[15], message[sig[6]], message[sig[7]]);

        mix(&workVector[0], &workVector[5], &workVector[10], &workVector[15], message[sig[8]], message[sig[9]]);
        mix(&workVector[1], &workVector[6], &workVector[11], &workVector[12], message[sig[10]], message[sig[11]]);
        mix(&workVector[2], &workVector[7], &workVector[8], &workVector[13], message[sig[12]], message[sig[13]]);
        mix(&workVector[3], &workVector[4], &workVector[9], &workVector[14], message[sig[14]], message[sig[15]]);
    }

    // internalHash is updated by being XORd with each half of the workvector. 
    for (i = 0; i < 8; i++) {
        internalHash[i] = internalHash[i] ^ workVector[i];;
    }
    for (i = 0; i < 8; i++) {
        internalHash[i] = internalHash[i] ^ workVector[i+8];
    }

    return internalHash;
}

/*
    Takes in two words 'm1' and 'm2' and mixes them into four words 'va', 'vb',
    'vc' and 'vd'. This void function aids the compression function by changing
    the arrzay element values of the pointers passed through. 
*/

void mix(uint32_t* va, uint32_t* vb, uint32_t* vc, uint32_t* vd, uint32_t m1, uint32_t m2) {
    // temp variable for any intermediate steps
    uint32_t temp;
    
    // following equations were derived from the psuedo code
    *va = (*va + *vb + m1);
    temp = (*vd ^ *va);
    *vd = (temp >> 16) | (temp << 16);

    *vc = (*vc + *vd);
    temp = (*vb ^ *vc);
    *vb = (temp >> 12) | (temp << 20);

    *va = (*va + *vb + m2);
    temp = (*vd ^ *va);
    *vd = (temp >> 8) | (temp << 24);

    *vc = (*vc + *vd);
    temp = (*vb ^ *vc);
    *vb = (temp >> 7) | (temp << 25);

    return;
}

uint32_t load32(char * message) {
    uint32_t word;
    memcpy(&word, message, sizeof(word));
    return word;
}